<?php
namespace BusinessManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * Employee Entity
 *
 * @property int $id
 * @property string $number
 * @property string $name
 * @property \Cake\I18n\FrozenDate|null $hire_date
 * @property int|null $department_id
 * @property int|null $business_id
 * @property string|null $notes
 * @property bool|null $active
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \BusinessManager\Model\Entity\Department $department
 * @property \BusinessManager\Model\Entity\Business $business
 */
class Employee extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'number' => true,
        'name' => true,
        'hire_date' => true,
        'department_id' => true,
        'business_id' => true,
        'notes' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'department' => true,
        'business' => true,
        'metadata_employee' => true
    ];
}
