<?php
namespace BusinessManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * MetadataEmployee Entity
 *
 * @property int $id
 * @property int|null $employee_id
 * @property string|null $address
 * @property string|null $city
 * @property string|null $postal_code
 * @property string|null $state
 * @property string|null $country
 * @property string|null $marital_status
 * @property string|null $nationality
 * @property string|null $country_of_birth
 * @property \Cake\I18n\FrozenDate|null $date_of_birth
 * @property string|null $identification_card
 * @property \Cake\I18n\FrozenDate|null $identification_card_date_of_issue
 * @property \Cake\I18n\FrozenDate|null $identification_card_validity
 * @property \Cake\I18n\FrozenDate|null $hiring_date
 * @property \Cake\I18n\FrozenDate|null $expiry_date
 * @property string|null $driving_licence
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \BusinessManager\Model\Entity\Employee $employee
 */
class MetadataEmployee extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'employee_id' => true,
        'address' => true,
        'city' => true,
        'postal_code' => true,
        'state' => true,
        'country' => true,
        'marital_status' => true,
        'nationality' => true,
        'country_of_birth' => true,
        'date_of_birth' => true,
        'identification_card' => true,
        'identification_card_date_of_issue' => true,
        'identification_card_validity' => true,
        'hiring_date' => true,
        'expiry_date' => true,
        'driving_licence' => true,
        'created' => true,
        'modified' => true,
        'employee' => true
    ];
}
