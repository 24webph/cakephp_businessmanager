<?php
namespace BusinessManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vehicle Entity
 *
 * @property int $id
 * @property string $description
 * @property string $registration
 * @property int|null $responsible_id
 * @property int|null $business_id
 * @property bool|null $active
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \BusinessManager\Model\Entity\Employee $employee
 * @property \BusinessManager\Model\Entity\Business $business
 */
class Vehicle extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'description' => true,
        'registration' => true,
        'responsible_id' => true,
        'business_id' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'employee' => true,
        'business' => true
    ];
}
