<?php
namespace BusinessManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * Business Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $fiscal_number
 * @property bool|null $active
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \BusinessManager\Model\Entity\Employee[] $employees
 * @property \BusinessManager\Model\Entity\Client[] $clients
 */
class Business extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'fiscal_number' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'addresses' => true,
        'employees' => true,
        'clients' => true
    ];
}
