<?php
namespace BusinessManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * EmployeesTeam Entity
 *
 * @property int $id
 * @property int $employee_id
 * @property int $team_id
 *
 * @property \BusinessManager\Model\Entity\Employee $employee
 * @property \BusinessManager\Model\Entity\Team $team
 */
class EmployeesTeam extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'employee_id' => true,
        'team_id' => true,
        'employee' => true,
        'team' => true
    ];
}
