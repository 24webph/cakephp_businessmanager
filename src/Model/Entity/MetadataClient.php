<?php
namespace BusinessManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * MetadataClient Entity
 *
 * @property int $id
 * @property int|null $client_id
 * @property string|null $address
 * @property string|null $district
 * @property string|null $county
 * @property string|null $parish
 * @property string|null $postal_code
 * @property \Cake\I18n\FrozenDate|null $hiring_date
 * @property \Cake\I18n\FrozenDate|null $cancel_date
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \BusinessManager\Model\Entity\Client $client
 */
class MetadataClient extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'client_id' => true,
        'address' => true,
        'locality' => true,
        'parish' => true,
        'county' => true,
        'district' => true,
        'postal_code' => true,
        'hiring_date' => true,
        'cancel_date' => true,
        'created' => true,
        'modified' => true,
        'client' => true
    ];
}
