<?php
namespace BusinessManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * BusinessesClient Entity
 *
 * @property int $id
 * @property int $business_id
 * @property int $client_id
 *
 * @property \BusinessManager\Model\Entity\Business $business
 * @property \BusinessManager\Model\Entity\Client $client
 */
class BusinessesClient extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'business_id' => true,
        'client_id' => true,
        'business' => true,
        'client' => true
    ];
}
