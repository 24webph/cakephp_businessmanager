<?php
namespace BusinessManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BusinessesClients Model
 *
 * @property \BusinessManager\Model\Table\BusinessesTable|\Cake\ORM\Association\BelongsTo $Businesses
 * @property \BusinessManager\Model\Table\ClientsTable|\Cake\ORM\Association\BelongsTo $Clients
 *
 * @method \BusinessManager\Model\Entity\BusinessesClient get($primaryKey, $options = [])
 * @method \BusinessManager\Model\Entity\BusinessesClient newEntity($data = null, array $options = [])
 * @method \BusinessManager\Model\Entity\BusinessesClient[] newEntities(array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\BusinessesClient|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\BusinessesClient saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\BusinessesClient patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\BusinessesClient[] patchEntities($entities, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\BusinessesClient findOrCreate($search, callable $callback = null, $options = [])
 */
class BusinessesClientsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('businesses_clients');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Businesses', [
            'foreignKey' => 'business_id',
            'joinType' => 'INNER',
            'className' => 'BusinessManager.Businesses'
        ]);
        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER',
            'className' => 'BusinessManager.Clients'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['business_id'], 'Businesses'));
        $rules->add($rules->existsIn(['client_id'], 'Clients'));

        return $rules;
    }
}
