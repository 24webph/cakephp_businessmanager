<?php
namespace BusinessManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmployeesTeams Model
 *
 * @property \BusinessManager\Model\Table\EmployeesTable|\Cake\ORM\Association\BelongsTo $Employees
 * @property \BusinessManager\Model\Table\TeamsTable|\Cake\ORM\Association\BelongsTo $Teams
 *
 * @method \BusinessManager\Model\Entity\EmployeesTeam get($primaryKey, $options = [])
 * @method \BusinessManager\Model\Entity\EmployeesTeam newEntity($data = null, array $options = [])
 * @method \BusinessManager\Model\Entity\EmployeesTeam[] newEntities(array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\EmployeesTeam|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\EmployeesTeam saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\EmployeesTeam patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\EmployeesTeam[] patchEntities($entities, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\EmployeesTeam findOrCreate($search, callable $callback = null, $options = [])
 */
class EmployeesTeamsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('employees_teams');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Employees', [
            'foreignKey' => 'employee_id',
            'joinType' => 'INNER',
            'className' => 'BusinessManager.Employees'
        ]);
        $this->belongsTo('Teams', [
            'foreignKey' => 'team_id',
            'joinType' => 'INNER',
            'className' => 'BusinessManager.Teams'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['employee_id'], 'Employees'));
        $rules->add($rules->existsIn(['team_id'], 'Teams'));

        return $rules;
    }
}
