<?php
namespace BusinessManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TeamTypes Model
 *
 * @property \BusinessManager\Model\Table\TeamsTable|\Cake\ORM\Association\HasMany $Teams
 *
 * @method \BusinessManager\Model\Entity\TeamType get($primaryKey, $options = [])
 * @method \BusinessManager\Model\Entity\TeamType newEntity($data = null, array $options = [])
 * @method \BusinessManager\Model\Entity\TeamType[] newEntities(array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\TeamType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\TeamType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\TeamType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\TeamType[] patchEntities($entities, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\TeamType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TeamTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('team_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Teams', [
            'foreignKey' => 'team_type_id',
            'className' => 'BusinessManager.Teams'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }
}
