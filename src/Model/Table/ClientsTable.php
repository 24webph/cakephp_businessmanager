<?php
namespace BusinessManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clients Model
 *
 * @property \BusinessManager\Model\Table\BusinessesTable|\Cake\ORM\Association\BelongsToMany $Businesses
 *
 * @method \BusinessManager\Model\Entity\Client get($primaryKey, $options = [])
 * @method \BusinessManager\Model\Entity\Client newEntity($data = null, array $options = [])
 * @method \BusinessManager\Model\Entity\Client[] newEntities(array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Client|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\Client saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\Client patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Client[] patchEntities($entities, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Client findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ClientsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('clients');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('ContactManager.Address');
        $this->addBehavior('ContactManager.Contact');
        
         $this->hasOne('MetadataClients');

        $this->belongsToMany('Businesses', [
            'foreignKey' => 'client_id',
            'targetForeignKey' => 'business_id',
            'joinTable' => 'businesses_clients',
            'className' => 'BusinessManager.Businesses'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->scalar('description')
            ->maxLength('description', 100)
            ->allowEmptyString('description');

        /*$validator
            ->scalar('address')
            ->allowEmptyString('address');*/

        $validator
            ->scalar('fiscal_number')
            ->maxLength('fiscal_number', 50)
            ->allowEmptyString('fiscal_number');
        
        $validator
            ->scalar('notes')
            ->allowEmptyString('notes');

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }
}
