<?php
namespace BusinessManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Teams Model
 *
 * @property \BusinessManager\Model\Table\EmployeesTable|\Cake\ORM\Association\BelongsTo $Employees
 * @property \BusinessManager\Model\Table\TeamTypesTable|\Cake\ORM\Association\BelongsTo $TeamTypes
 * @property \BusinessManager\Model\Table\EmployeesTable|\Cake\ORM\Association\BelongsToMany $Employees
 *
 * @method \BusinessManager\Model\Entity\Team get($primaryKey, $options = [])
 * @method \BusinessManager\Model\Entity\Team newEntity($data = null, array $options = [])
 * @method \BusinessManager\Model\Entity\Team[] newEntities(array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Team|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\Team saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\Team patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Team[] patchEntities($entities, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Team findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TeamsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('teams');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Employees', [
            'foreignKey' => 'manager_id',
            'className' => 'BusinessManager.Employees'
        ]);
        $this->belongsTo('TeamTypes', [
            'foreignKey' => 'team_type_id',
            'className' => 'BusinessManager.TeamTypes'
        ]);
        $this->belongsToMany('Employees', [
            'foreignKey' => 'team_id',
            'targetForeignKey' => 'employee_id',
            'joinTable' => 'employees_teams',
            'className' => 'BusinessManager.Employees'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['manager_id'], 'Employees'));
        $rules->add($rules->existsIn(['team_type_id'], 'TeamTypes'));

        return $rules;
    }
}
