<?php
namespace BusinessManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Employees Model
 *
 * @property \BusinessManager\Model\Table\DepartmentsTable|\Cake\ORM\Association\BelongsTo $Departments
 * @property \BusinessManager\Model\Table\BusinessesTable|\Cake\ORM\Association\BelongsTo $Businesses
 * @property |\Cake\ORM\Association\BelongsToMany $Teams
 *
 * @method \BusinessManager\Model\Entity\Employee get($primaryKey, $options = [])
 * @method \BusinessManager\Model\Entity\Employee newEntity($data = null, array $options = [])
 * @method \BusinessManager\Model\Entity\Employee[] newEntities(array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Employee|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\Employee saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\Employee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Employee[] patchEntities($entities, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Employee findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmployeesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('employees');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        
        $this->hasOne('MetadataEmployees');

        $this->belongsTo('Departments', [
            'foreignKey' => 'department_id',
            'className' => 'BusinessManager.Departments'
        ]);
        $this->belongsTo('Businesses', [
            'foreignKey' => 'business_id',
            'className' => 'BusinessManager.Businesses'
        ]);
        $this->belongsToMany('Teams', [
            'foreignKey' => 'employee_id',
            'targetForeignKey' => 'team_id',
            'joinTable' => 'employees_teams',
            'className' => 'BusinessManager.Teams'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('number')
            ->maxLength('number', 36)
            ->requirePresence('number', 'create')
            ->allowEmptyString('number', false);

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->date('hire_date')
            ->allowEmptyDate('hire_date');

        $validator
            ->scalar('notes')
            ->allowEmptyString('notes');

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['department_id'], 'Departments'));
        $rules->add($rules->existsIn(['business_id'], 'Businesses'));

        return $rules;
    }
}
