<?php
namespace BusinessManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Vehicles Model
 *
 * @property \BusinessManager\Model\Table\EmployeesTable|\Cake\ORM\Association\BelongsTo $Employees
 * @property \BusinessManager\Model\Table\BusinessesTable|\Cake\ORM\Association\BelongsTo $Businesses
 *
 * @method \BusinessManager\Model\Entity\Vehicle get($primaryKey, $options = [])
 * @method \BusinessManager\Model\Entity\Vehicle newEntity($data = null, array $options = [])
 * @method \BusinessManager\Model\Entity\Vehicle[] newEntities(array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Vehicle|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\Vehicle saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\Vehicle patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Vehicle[] patchEntities($entities, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Vehicle findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class VehiclesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vehicles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Employees', [
            'foreignKey' => 'responsible_id',
            'className' => 'BusinessManager.Employees'
        ]);
        $this->belongsTo('Businesses', [
            'foreignKey' => 'business_id',
            'className' => 'BusinessManager.Businesses'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('description')
            ->maxLength('description', 100)
            ->requirePresence('description', 'create')
            ->allowEmptyString('description', false);

        $validator
            ->scalar('registration')
            ->maxLength('registration', 100)
            ->requirePresence('registration', 'create')
            ->allowEmptyString('registration', false);

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['responsible_id'], 'Employees'));
        $rules->add($rules->existsIn(['business_id'], 'Businesses'));

        return $rules;
    }
}
