<?php
namespace BusinessManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Businesses Model
 *
 * @property \BusinessManager\Model\Table\EmployeesTable|\Cake\ORM\Association\HasMany $Employees
 * @property \BusinessManager\Model\Table\ClientsTable|\Cake\ORM\Association\BelongsToMany $Clients
 *
 * @method \BusinessManager\Model\Entity\Business get($primaryKey, $options = [])
 * @method \BusinessManager\Model\Entity\Business newEntity($data = null, array $options = [])
 * @method \BusinessManager\Model\Entity\Business[] newEntities(array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Business|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\Business saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\Business patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Business[] patchEntities($entities, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\Business findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BusinessesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('businesses');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('ContactManager.Address');

        $this->hasMany('Employees', [
            'foreignKey' => 'business_id',
            'className' => 'BusinessManager.Employees'
        ]);
        
        $this->belongsToMany('Clients', [
            'foreignKey' => 'business_id',
            'targetForeignKey' => 'client_id',
            'joinTable' => 'businesses_clients',
            'className' => 'BusinessManager.Clients'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->scalar('description')
            ->maxLength('description', 100)
            ->allowEmptyString('description');

        /*$validator
            ->scalar('address')
            ->allowEmptyString('address');*/

        $validator
            ->scalar('fiscal_number')
            ->maxLength('fiscal_number', 50)
            ->allowEmptyString('fiscal_number');

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }
    
    // total stock virtual field
    protected function _getTotalEmployess() {
        $stock = 0;
        $stock = $this->consumablesCounter();
        return $stock;
    }

    private function consumablesCounter() {

        $stockTable = TableRegistry::get('Employees');
        $query = $stockTable->find()->where(['Employees.business_id' => $this->_properties['id']]);
        $sum = $query->where(['active'=>true])->select(['sumOfEmployees' => $query->func()->count()])->first();
        return $sum->sumOfEmployees;
    }
}
