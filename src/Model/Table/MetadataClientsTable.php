<?php
namespace BusinessManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MetadataClients Model
 *
 * @property \BusinessManager\Model\Table\ClientsTable|\Cake\ORM\Association\BelongsTo $Clients
 *
 * @method \BusinessManager\Model\Entity\MetadataClient get($primaryKey, $options = [])
 * @method \BusinessManager\Model\Entity\MetadataClient newEntity($data = null, array $options = [])
 * @method \BusinessManager\Model\Entity\MetadataClient[] newEntities(array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\MetadataClient|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\MetadataClient saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\MetadataClient patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\MetadataClient[] patchEntities($entities, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\MetadataClient findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MetadataClientsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('metadata_clients');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'className' => 'BusinessManager.Clients'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('address')
            ->maxLength('address', 400)
            ->allowEmptyString('address');
        
        $validator
            ->scalar('locality')
            ->maxLength('locality', 100)
            ->allowEmptyString('locality');
        
        $validator
            ->scalar('parish')
            ->maxLength('parish', 100)
            ->allowEmptyString('parish');
        
        $validator
            ->scalar('county')
            ->maxLength('county', 100)
            ->allowEmptyString('county');

        $validator
            ->scalar('district')
            ->maxLength('district', 100)
            ->allowEmptyString('district');

        
        $validator
            ->scalar('postal_code')
            ->maxLength('postal_code', 100)
            ->allowEmptyString('postal_code');

        $validator
            ->date('hiring_date')
            ->allowEmptyDate('hiring_date');

        $validator
            ->date('cancel_date')
            ->allowEmptyDate('cancel_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['client_id'], 'Clients'));

        return $rules;
    }
}
