<?php
namespace BusinessManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MetadataEmployees Model
 *
 * @property \BusinessManager\Model\Table\EmployeesTable|\Cake\ORM\Association\BelongsTo $Employees
 *
 * @method \BusinessManager\Model\Entity\MetadataEmployee get($primaryKey, $options = [])
 * @method \BusinessManager\Model\Entity\MetadataEmployee newEntity($data = null, array $options = [])
 * @method \BusinessManager\Model\Entity\MetadataEmployee[] newEntities(array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\MetadataEmployee|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\MetadataEmployee saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \BusinessManager\Model\Entity\MetadataEmployee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\MetadataEmployee[] patchEntities($entities, array $data, array $options = [])
 * @method \BusinessManager\Model\Entity\MetadataEmployee findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MetadataEmployeesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('metadata_employees');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Employees', [
            'foreignKey' => 'employee_id',
            'className' => 'BusinessManager.Employees'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('address')
            ->maxLength('address', 400)
            ->allowEmptyString('address');

        $validator
            ->scalar('city')
            ->maxLength('city', 100)
            ->allowEmptyString('city');

        $validator
            ->scalar('postal_code')
            ->maxLength('postal_code', 100)
            ->allowEmptyString('postal_code');

        $validator
            ->scalar('state')
            ->maxLength('state', 100)
            ->allowEmptyString('state');

        $validator
            ->scalar('country')
            ->maxLength('country', 100)
            ->allowEmptyString('country');

        $validator
            ->scalar('marital_status')
            ->maxLength('marital_status', 50)
            ->allowEmptyString('marital_status');

        $validator
            ->scalar('nationality')
            ->maxLength('nationality', 50)
            ->allowEmptyString('nationality');

        $validator
            ->scalar('country_of_birth')
            ->maxLength('country_of_birth', 50)
            ->allowEmptyString('country_of_birth');

        $validator
            ->date('date_of_birth')
            ->allowEmptyDate('date_of_birth');

        $validator
            ->scalar('identification_card')
            ->maxLength('identification_card', 50)
            ->allowEmptyString('identification_card');

        $validator
            ->date('identification_card_date_of_issue')
            ->allowEmptyDate('identification_card_date_of_issue');

        $validator
            ->date('identification_card_validity')
            ->allowEmptyDate('identification_card_validity');

        $validator
            ->date('hiring_date')
            ->allowEmptyDate('hiring_date');

        $validator
            ->date('expiry_date')
            ->allowEmptyDate('expiry_date');

        $validator
            ->scalar('driving_licence')
            ->maxLength('driving_licence', 50)
            ->allowEmptyString('driving_licence');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['employee_id'], 'Employees'));

        return $rules;
    }
}
