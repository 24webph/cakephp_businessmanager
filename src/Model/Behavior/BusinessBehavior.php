<?php

/**
 * BusinessManagerBehavior
 *
 * PHP version 7
 * 
 * @category Behaviour
 * @package  24WEB.BusinessManager
 * @version  V1
 * @author   Paulo Homem <paulo.homem@24web.pt>
 * @license  
 * @link     http://24web.pt
 */

namespace BusinessManager\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Event\Event;
use ArrayObject;
use Cake\ORM\RulesChecker;

class BusinessBehavior extends Behavior {

    public function initialize(array $config) {
        $this->setTableSettings();
    }

    private function setTableSettings() {
        $this->getTable()->belongsTo('Businesses', [
            'foreignKey' => 'business_id',
            'className' => 'BusinessManager.Businesses'
        ]);
        $this->getTable()->belongsTo('Managers', [
            'foreignKey' => 'manager_id',
            'className' => 'BusinessManager.Employees'
        ]);
        
    }
    
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(Event $event, RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['business_id'], 'Businesses'));
        $rules->add($rules->existsIn(['manager_id'], 'Managers'));
        return $rules;
    }

}
