<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $metadataEmployee
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Metadata Employee'), ['action' => 'edit', $metadataEmployee->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Metadata Employee'), ['action' => 'delete', $metadataEmployee->id], ['confirm' => __('Are you sure you want to delete # {0}?', $metadataEmployee->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Metadata Employees'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Metadata Employee'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="metadataEmployees view large-9 medium-8 columns content">
    <h3><?= h($metadataEmployee->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Employee') ?></th>
            <td><?= $metadataEmployee->has('employee') ? $this->Html->link($metadataEmployee->employee->name, ['controller' => 'Employees', 'action' => 'view', $metadataEmployee->employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($metadataEmployee->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('City') ?></th>
            <td><?= h($metadataEmployee->city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Postal Code') ?></th>
            <td><?= h($metadataEmployee->postal_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('State') ?></th>
            <td><?= h($metadataEmployee->state) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country') ?></th>
            <td><?= h($metadataEmployee->country) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Marital Status') ?></th>
            <td><?= h($metadataEmployee->marital_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nationality') ?></th>
            <td><?= h($metadataEmployee->nationality) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country Of Birth') ?></th>
            <td><?= h($metadataEmployee->country_of_birth) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Identification Card') ?></th>
            <td><?= h($metadataEmployee->identification_card) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Driving Licence') ?></th>
            <td><?= h($metadataEmployee->driving_licence) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($metadataEmployee->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Of Birth') ?></th>
            <td><?= h($metadataEmployee->date_of_birth) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Identification Card Date Of Issue') ?></th>
            <td><?= h($metadataEmployee->identification_card_date_of_issue) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Identification Card Validity') ?></th>
            <td><?= h($metadataEmployee->identification_card_validity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hiring Date') ?></th>
            <td><?= h($metadataEmployee->hiring_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expiry Date') ?></th>
            <td><?= h($metadataEmployee->expiry_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($metadataEmployee->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($metadataEmployee->modified) ?></td>
        </tr>
    </table>
</div>
