<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $metadataEmployees
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Metadata Employee'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="metadataEmployees index large-9 medium-8 columns content">
    <h3><?= __('Metadata Employees') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('employee_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('city') ?></th>
                <th scope="col"><?= $this->Paginator->sort('postal_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('state') ?></th>
                <th scope="col"><?= $this->Paginator->sort('country') ?></th>
                <th scope="col"><?= $this->Paginator->sort('marital_status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nationality') ?></th>
                <th scope="col"><?= $this->Paginator->sort('country_of_birth') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_of_birth') ?></th>
                <th scope="col"><?= $this->Paginator->sort('identification_card') ?></th>
                <th scope="col"><?= $this->Paginator->sort('identification_card_date_of_issue') ?></th>
                <th scope="col"><?= $this->Paginator->sort('identification_card_validity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('hiring_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('expiry_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('driving_licence') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($metadataEmployees as $metadataEmployee): ?>
            <tr>
                <td><?= $this->Number->format($metadataEmployee->id) ?></td>
                <td><?= $metadataEmployee->has('employee') ? $this->Html->link($metadataEmployee->employee->name, ['controller' => 'Employees', 'action' => 'view', $metadataEmployee->employee->id]) : '' ?></td>
                <td><?= h($metadataEmployee->address) ?></td>
                <td><?= h($metadataEmployee->city) ?></td>
                <td><?= h($metadataEmployee->postal_code) ?></td>
                <td><?= h($metadataEmployee->state) ?></td>
                <td><?= h($metadataEmployee->country) ?></td>
                <td><?= h($metadataEmployee->marital_status) ?></td>
                <td><?= h($metadataEmployee->nationality) ?></td>
                <td><?= h($metadataEmployee->country_of_birth) ?></td>
                <td><?= h($metadataEmployee->date_of_birth) ?></td>
                <td><?= h($metadataEmployee->identification_card) ?></td>
                <td><?= h($metadataEmployee->identification_card_date_of_issue) ?></td>
                <td><?= h($metadataEmployee->identification_card_validity) ?></td>
                <td><?= h($metadataEmployee->hiring_date) ?></td>
                <td><?= h($metadataEmployee->expiry_date) ?></td>
                <td><?= h($metadataEmployee->driving_licence) ?></td>
                <td><?= h($metadataEmployee->created) ?></td>
                <td><?= h($metadataEmployee->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $metadataEmployee->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $metadataEmployee->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $metadataEmployee->id], ['confirm' => __('Are you sure you want to delete # {0}?', $metadataEmployee->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
