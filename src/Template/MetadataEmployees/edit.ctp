<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $metadataEmployee
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $metadataEmployee->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $metadataEmployee->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Metadata Employees'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="metadataEmployees form large-9 medium-8 columns content">
    <?= $this->Form->create($metadataEmployee) ?>
    <fieldset>
        <legend><?= __('Edit Metadata Employee') ?></legend>
        <?php
            echo $this->Form->control('employee_id', ['options' => $employees, 'empty' => true]);
            echo $this->Form->control('address');
            echo $this->Form->control('city');
            echo $this->Form->control('postal_code');
            echo $this->Form->control('state');
            echo $this->Form->control('country');
            echo $this->Form->control('marital_status');
            echo $this->Form->control('nationality');
            echo $this->Form->control('country_of_birth');
            echo $this->Form->control('date_of_birth', ['empty' => true]);
            echo $this->Form->control('identification_card');
            echo $this->Form->control('identification_card_date_of_issue', ['empty' => true]);
            echo $this->Form->control('identification_card_validity', ['empty' => true]);
            echo $this->Form->control('hiring_date', ['empty' => true]);
            echo $this->Form->control('expiry_date', ['empty' => true]);
            echo $this->Form->control('driving_licence');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
