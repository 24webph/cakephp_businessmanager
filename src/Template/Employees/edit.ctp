<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $employee
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $employee->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $employee->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Departments'), ['controller' => 'Departments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Department'), ['controller' => 'Departments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Businesses'), ['controller' => 'Businesses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Business'), ['controller' => 'Businesses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Metadata Employees'), ['controller' => 'MetadataEmployees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Metadata Employee'), ['controller' => 'MetadataEmployees', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Teams'), ['controller' => 'Teams', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Team'), ['controller' => 'Teams', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="employees form large-9 medium-8 columns content">
    <?= $this->Form->create($employee) ?>
    <fieldset>
        <legend><?= __('Edit Employee') ?></legend>
        <?php
            echo $this->Form->control('number');
            echo $this->Form->control('name');
            echo $this->Form->control('department_id', ['options' => $departments, 'empty' => true]);
            echo $this->Form->control('business_id', ['options' => $businesses, 'empty' => true]);
            echo $this->Form->control('notes');
            echo $this->Form->control('active');
            echo $this->Form->control('teams._ids', ['options' => $teams]);
        ?>
    </fieldset>
    <fieldset>
        <legend><?= __('Edit Metadata Employee') ?></legend>
        <?php
            echo $this->Form->hidden('metadata_employee.employee_id',['value'=>$employee->id]);
            echo $this->Form->control('metadata_employee.address');
            echo $this->Form->control('metadata_employee.city');
            echo $this->Form->control('metadata_employee.postal_code');
            echo $this->Form->control('metadata_employee.state');
            echo $this->Form->control('metadata_employee.country');
            echo $this->Form->control('metadata_employee.marital_status');
            echo $this->Form->control('metadata_employee.nationality');
            echo $this->Form->control('metadata_employee.country_of_birth');
            echo $this->Form->control('metadata_employee.date_of_birth', ['empty' => true]);
            echo $this->Form->control('metadata_employee.identification_card');
            echo $this->Form->control('metadata_employee.identification_card_date_of_issue', ['empty' => true]);
            echo $this->Form->control('metadata_employee.identification_card_validity', ['empty' => true]);
            echo $this->Form->control('metadata_employee.hiring_date', ['empty' => true]);
            echo $this->Form->control('metadata_employee.expiry_date', ['empty' => true]);
            echo $this->Form->control('metadata_employee.driving_licence');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
