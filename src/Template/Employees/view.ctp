<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $employee
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Employee'), ['action' => 'edit', $employee->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Employee'), ['action' => 'delete', $employee->id], ['confirm' => __('Are you sure you want to delete # {0}?', $employee->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Employees'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Employee'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Departments'), ['controller' => 'Departments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Department'), ['controller' => 'Departments', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Businesses'), ['controller' => 'Businesses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Business'), ['controller' => 'Businesses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Metadata Employees'), ['controller' => 'MetadataEmployees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Metadata Employee'), ['controller' => 'MetadataEmployees', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Teams'), ['controller' => 'Teams', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Team'), ['controller' => 'Teams', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="employees view large-9 medium-8 columns content">
    <h3><?= h($employee->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Number') ?></th>
            <td><?= h($employee->number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($employee->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Department') ?></th>
            <td><?= $employee->has('department') ? $this->Html->link($employee->department->name, ['controller' => 'Departments', 'action' => 'view', $employee->department->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Business') ?></th>
            <td><?= $employee->has('business') ? $this->Html->link($employee->business->name, ['controller' => 'Businesses', 'action' => 'view', $employee->business->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Metadata Employee') ?></th>
            <td><?= $employee->has('metadata_employee') ? $this->Html->link($employee->metadata_employee->id, ['controller' => 'MetadataEmployees', 'action' => 'view', $employee->metadata_employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($employee->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($employee->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($employee->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $employee->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($employee->notes)); ?>
    </div>
    <div class="row">
    <h4><?= __('Metadata') ?></h4>
        <?php if ($employee->has('metadata_employee')): ?>
        <?php $metadataEmployee=$employee->metadata_employee; ?>
            <div class="metadataEmployees view large-9 medium-8 columns content">
                <table class="vertical-table">
                    <tr>
                        <th scope="row"><?= __('Address') ?></th>
                        <td><?= h($metadataEmployee->address) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('City') ?></th>
                        <td><?= h($metadataEmployee->city) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Postal Code') ?></th>
                        <td><?= h($metadataEmployee->postal_code) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('State') ?></th>
                        <td><?= h($metadataEmployee->state) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Country') ?></th>
                        <td><?= h($metadataEmployee->country) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Marital Status') ?></th>
                        <td><?= h($metadataEmployee->marital_status) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Nationality') ?></th>
                        <td><?= h($metadataEmployee->nationality) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Country Of Birth') ?></th>
                        <td><?= h($metadataEmployee->country_of_birth) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Identification Card') ?></th>
                        <td><?= h($metadataEmployee->identification_card) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Driving Licence') ?></th>
                        <td><?= h($metadataEmployee->driving_licence) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Id') ?></th>
                        <td><?= $this->Number->format($metadataEmployee->id) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Date Of Birth') ?></th>
                        <td><?= h($metadataEmployee->date_of_birth) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Identification Card Date Of Issue') ?></th>
                        <td><?= h($metadataEmployee->identification_card_date_of_issue) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Identification Card Validity') ?></th>
                        <td><?= h($metadataEmployee->identification_card_validity) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Hiring Date') ?></th>
                        <td><?= h($metadataEmployee->hiring_date) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Expiry Date') ?></th>
                        <td><?= h($metadataEmployee->expiry_date) ?></td>
                    </tr>
                    
                </table>
            </div>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Teams') ?></h4>
        <?php if (!empty($employee->teams)): ?>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Team Type Id') ?></th>
                    <th scope="col"><?= __('Active') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($employee->teams as $teams): ?>
                    <tr>
                        <td><?= h($teams->id) ?></td>
                        <td><?= h($teams->name) ?></td>
                        <td><?= h($teams->team_type_id) ?></td>
                        <td><?= h($teams->active) ?></td>
                        <td><?= h($teams->created) ?></td>
                        <td><?= h($teams->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['controller' => 'Teams', 'action' => 'view', $teams->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['controller' => 'Teams', 'action' => 'edit', $teams->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Teams', 'action' => 'delete', $teams->id], ['confirm' => __('Are you sure you want to delete # {0}?', $teams->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>
