<?php
namespace BusinessManager\Controller;

use BusinessManager\Controller\AppController;

/**
 * TeamTypes Controller
 *
 * @property \BusinessManager\Model\Table\TeamTypesTable $TeamTypes
 *
 * @method \BusinessManager\Model\Entity\TeamType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TeamTypesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $teamTypes = $this->paginate($this->TeamTypes);

        $this->set(compact('teamTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Team Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $teamType = $this->TeamTypes->get($id, [
            'contain' => ['Teams']
        ]);

        $this->set('teamType', $teamType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $teamType = $this->TeamTypes->newEntity();
        if ($this->request->is('post')) {
            $teamType = $this->TeamTypes->patchEntity($teamType, $this->request->getData());
            if ($this->TeamTypes->save($teamType)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$teamType->id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $this->set(compact('teamType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Team Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $teamType = $this->TeamTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $teamType = $this->TeamTypes->patchEntity($teamType, $this->request->getData());
            if ($this->TeamTypes->save($teamType)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $this->set(compact('teamType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Team Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $teamType = $this->TeamTypes->get($id);
        if ($this->TeamTypes->delete($teamType)) {
            $this->Flash->success(__('Deleted with success.'));
        } else {
            $this->Flash->error(__('Could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
