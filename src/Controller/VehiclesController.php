<?php
namespace BusinessManager\Controller;

use BusinessManager\Controller\AppController;

/**
 * Vehicles Controller
 *
 * @property \BusinessManager\Model\Table\VehiclesTable $Vehicles
 *
 * @method \BusinessManager\Model\Entity\Vehicle[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VehiclesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Employees', 'Businesses']
        ];
        $vehicles = $this->paginate($this->Vehicles);

        $this->set(compact('vehicles'));
    }

    /**
     * View method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vehicle = $this->Vehicles->get($id, [
            'contain' => ['Employees', 'Businesses']
        ]);

        $this->set('vehicle', $vehicle);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vehicle = $this->Vehicles->newEntity();
        if ($this->request->is('post')) {
            $vehicle = $this->Vehicles->patchEntity($vehicle, $this->request->getData());
            if ($this->Vehicles->save($vehicle)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$vehicle->id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $employees = $this->Vehicles->Employees->find('list', ['limit' => 200]);
        $businesses = $this->Vehicles->Businesses->find('list', ['limit' => 200]);
        $this->set(compact('vehicle', 'employees', 'businesses'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vehicle = $this->Vehicles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vehicle = $this->Vehicles->patchEntity($vehicle, $this->request->getData());
            if ($this->Vehicles->save($vehicle)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $employees = $this->Vehicles->Employees->find('list', ['limit' => 200]);
        $businesses = $this->Vehicles->Businesses->find('list', ['limit' => 200]);
        $this->set(compact('vehicle', 'employees', 'businesses'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vehicle = $this->Vehicles->get($id);
        if ($this->Vehicles->delete($vehicle)) {
            $this->Flash->success(__('Deleted with success.'));
        } else {
            $this->Flash->error(__('Could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
