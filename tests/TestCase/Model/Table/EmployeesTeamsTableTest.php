<?php
namespace BusinessManager\Test\TestCase\Model\Table;

use BusinessManager\Model\Table\EmployeesTeamsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * BusinessManager\Model\Table\EmployeesTeamsTable Test Case
 */
class EmployeesTeamsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \BusinessManager\Model\Table\EmployeesTeamsTable
     */
    public $EmployeesTeams;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.BusinessManager.EmployeesTeams',
        'plugin.BusinessManager.Employees',
        'plugin.BusinessManager.Teams'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('EmployeesTeams') ? [] : ['className' => EmployeesTeamsTable::class];
        $this->EmployeesTeams = TableRegistry::getTableLocator()->get('EmployeesTeams', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EmployeesTeams);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
