<?php
namespace BusinessManager\Test\TestCase\Model\Table;

use BusinessManager\Model\Table\BusinessesClientsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * BusinessManager\Model\Table\BusinessesClientsTable Test Case
 */
class BusinessesClientsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \BusinessManager\Model\Table\BusinessesClientsTable
     */
    public $BusinessesClients;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.BusinessManager.BusinessesClients',
        'plugin.BusinessManager.Businesses',
        'plugin.BusinessManager.Clients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('BusinessesClients') ? [] : ['className' => BusinessesClientsTable::class];
        $this->BusinessesClients = TableRegistry::getTableLocator()->get('BusinessesClients', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BusinessesClients);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
