<?php
namespace BusinessManager\Test\TestCase\Model\Table;

use BusinessManager\Model\Table\TeamTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * BusinessManager\Model\Table\TeamTypesTable Test Case
 */
class TeamTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \BusinessManager\Model\Table\TeamTypesTable
     */
    public $TeamTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.BusinessManager.TeamTypes',
        'plugin.BusinessManager.Teams'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TeamTypes') ? [] : ['className' => TeamTypesTable::class];
        $this->TeamTypes = TableRegistry::getTableLocator()->get('TeamTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TeamTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
