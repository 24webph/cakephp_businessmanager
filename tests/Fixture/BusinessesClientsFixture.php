<?php
namespace BusinessManager\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BusinessesClientsFixture
 */
class BusinessesClientsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'business_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'client_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'businesses_clients_businesses_fk' => ['type' => 'index', 'columns' => ['business_id'], 'length' => []],
            'businesses_clients_clients_fk' => ['type' => 'index', 'columns' => ['client_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'businesses_clients_businesses_fk' => ['type' => 'foreign', 'columns' => ['business_id'], 'references' => ['businesses', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'businesses_clients_clients_fk' => ['type' => 'foreign', 'columns' => ['client_id'], 'references' => ['clients', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'business_id' => 1,
                'client_id' => 1
            ],
        ];
        parent::init();
    }
}
